import React from 'react';
import { InputLabel, TextField } from '@material-ui/core';
import './TextField.css';


const TextFieldComponent = ({ name, valueInput, row, errorInput,
    callback, InputProps, placeholder = '', title = '', className }) => {

    return (
        <>
            {title &&
                <InputLabel className='input-label-title'>
                    {title}
                </InputLabel>
            }
            <TextField
                id={`filled-basic ${name}`}
                variant="filled"
                margin="normal"
                required
                fullWidth
                name={name}
                value={valueInput ? valueInput : ""}
                autoComplete={name}
                onChange={callback}
                className={`input-texfield ${className}`}
                multiline={!!row}
                rows={row}
                rowsMax={row}
                error={!!errorInput}
                helperText={errorInput}
                InputProps={InputProps}
                placeholder={placeholder}
            />
        </>
    );
};

export default TextFieldComponent;