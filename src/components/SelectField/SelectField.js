import React from 'react';
import { TextField, InputLabel, MenuItem } from '@material-ui/core';
import './SelectField.css';

const SelectField = ({ name, valueInput, errorInput, simple = false,
    onChange, placeholder = '', title = '', options = [] }) => {

    return (
        <>
            {title &&
                <InputLabel className='select-field-label-title'>
                    {title}
                </InputLabel>
            }
            <TextField
                variant='filled'
                margin='normal'
                select
                name={name}
                value={valueInput ? valueInput : 'none'}
                onChange={onChange}
                error={!!errorInput}
                helperText={errorInput}
                className='select-text-field'
            >
                <MenuItem value='none' disabled className='select-field-placeholder'>
                    {placeholder}
                </MenuItem>
                {simple ?
                    options.map((option) => (
                        <MenuItem key={option} value={option}>
                            {option}
                        </MenuItem>
                    ))
                    :
                    options.map((option) => (
                        <MenuItem key={option.key} value={option.key}>
                            {option.value}
                        </MenuItem>
                    ))
                }
            </TextField>
        </>
    );
};

export default SelectField;