import { firestore } from '../services/Firebase';

// constants
const initialState = {
    cars: [],
    car: {},
    isEditedCreated: false,
    isCreating: true,
    colors: []
};
var unsubscribe = null;
// types
const LOADING_CAR = 'LOADING_CAR';
const CAR_ERROR = 'CAR_ERROR';
const IS_CREATING_CAR = 'IS_CREATING_CAR';
const GET_CARS_SUCCESS = 'GET_CARS_SUCCESS';
const COLORS_SUCCESS = 'COLORS_SUCCESS';
const CAR_CREATE_UPDATE = 'CAR_CREATE_UPDATE';
const GET_CAR_BY_ID_SUCCESS = 'GET_CAR_BY_ID_SUCCESS';
const CANCEL_CREATED_UPDATED_CAR = 'CANCEL_CREATED_UPDATED_CAR';
// reducer
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOADING_CAR:
            return { ...state, loading: true, error: null };
        case IS_CREATING_CAR:
            return {
                ...state, isCreating: action.payload,
                isEditedCreated: false,
            };
        case GET_CARS_SUCCESS:
            return {
                ...state, cars: action.payload,
                error: null, loading: false, car: {}
            };
        case COLORS_SUCCESS:
            return {
                ...state, colors: action.payload,
                loading: false, error: null
            };
        case CAR_CREATE_UPDATE:
            return {
                ...state, car: {}, isEditedCreated: true,
                loading: false, error: null
            };
        case CANCEL_CREATED_UPDATED_CAR:
            return {
                ...state, isEditedCreated: false
            };
        case GET_CAR_BY_ID_SUCCESS:
            return {
                ...state, car: action.payload,
                loading: false, error: null
            };
        case CAR_ERROR:
            return {
                ...state, error: action.payload, loading: false
            };
        default:
            return state;
    }

}


//Actions

export const changeIsCreating = (state) => (dispatch) => {
    dispatch({
        type: IS_CREATING_CAR,
        payload: state
    });
};

export const getCarList = () => async (dispatch) => {
    // dispatch({ type: LOADING_CAR });
    try {
        if (unsubscribe)
            unsubscribe();
        unsubscribe =
            firestore.collection('cars/').onSnapshot(querySnapshot => {
                dispatch({
                    type: GET_CARS_SUCCESS,
                    payload: onCollectionUpdate(querySnapshot)
                });
            });
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: 'Error al cargar lista de carros'
        });
    }
};

const onCollectionUpdate = (querySnapshot) => {
    const carsList = [];

    querySnapshot.forEach(doc => {
        const data = doc.data();

        carsList.push({
            key: doc.id,
            plaque: data['plaque'],
            price: data['price'],
            color: data['color'].value,
            model: data['model'],
            status: data['status'],
            complete: data['plaque'] + ' | ' + data['color'].value + ' | ' + data['model'] + ' | ' + data['price'] + ' | ' + data['status']
        });
    });
    return carsList;
};


export const getColors = () => async (dispatch) => {
    try {
        const ColorsList = [];
        const Colors = await firestore.collection('colors').get();
        Colors.forEach(item => {
            ColorsList.push({
                key: item.id,
                value: item.data().name
            });
        });
        dispatch({
            type: COLORS_SUCCESS,
            payload: ColorsList
        })
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: 'Error al cargar los colores'
        })
    }
};

export const addDataCars = (car) => async (dispatch, getState) => {
    try {
        const { isCreating } = getState().car;
        const carRedux = getState().car.car;
        const { colors } = getState().car;
        const carColor = colors.find(item => item.key === car.colors);

        const newCar = {
            'plaque': car.plaque,
            'model': car.model,
            'price': parseFloat(car.price),
            'color': { key: carColor.key, value: carColor.value },
            'status': car.status === 'new' ? 'Nuevo' : 'Usado',
        }
        if (isCreating) {
            await firestore.collection('cars').add(newCar);
        } else
            await firestore.doc(`cars/${carRedux.key}`).update(newCar);
            
        dispatch({
            type: CAR_CREATE_UPDATE,
            payload: "Usuario agregado correctamente"
        });
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: "Error al agregar el nuevo carro"
        });
    }
}

export const cancelCarCreatedUpdated = () => (dispatch) => {
    dispatch({
        type: CANCEL_CREATED_UPDATED_CAR,
    });
};

export const getCarById = (carKey) => async (dispatch) => {
    console.log('UPDATE' + carKey);
    try {
        const car = await firestore.doc(`cars/${carKey}`).get();

        if (car.exists) {
            const carObject = {
                key: car.id,
                plaque: car.data().plaque,
                price: car.data().price,
                color: car.data()['color'].key,
                model: car.data()['model'],
                status: car.data()['status'],
            };
            dispatch({
                type: GET_CAR_BY_ID_SUCCESS,
                payload: carObject
            });
        }
    } catch (error) {
        dispatch({
            type: CAR_ERROR,
            payload: 'Error al cargar el carro'
        });
    }
};