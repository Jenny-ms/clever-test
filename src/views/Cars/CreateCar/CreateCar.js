import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Form from '../Form';
import { changeIsCreating } from '../../../redux/carsDuck';

const CreateCar = () => {

    const isCreating = useSelector(store => store.car.isCreating);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(changeIsCreating(true));
    }, [dispatch]);

    return isCreating && (
        <Form />
    );
};

export default CreateCar;