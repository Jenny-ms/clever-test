import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Form from '../Form';
import { changeIsCreating, getCarById } from '../../../redux/carsDuck';

const UpdateCar = props => {

    const isCreating = useSelector(store => store.car.isCreating);
    const dispatch = useDispatch();

    useEffect(() => {
        const { id } = props.match.params;
        console.log(id);
        if (id) {
            dispatch(changeIsCreating(false));
            dispatch(getCarById(id));
        }
    }, [dispatch, props.match.params]);

    return !isCreating && (
        <Form />
    );
};

export default withRouter(UpdateCar);