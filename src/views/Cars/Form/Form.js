import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch, } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { changeIsCreating, getColors, addDataCars, cancelCarCreatedUpdated, getCarById } from '../../../redux/carsDuck';
import TextFieldComponent from '../../../components/TextField';
import SelectFieldComponent from '../../../components/SelectField';
import { Button, Grid, FormControlLabel, Switch, Snackbar } from '@material-ui/core';
import { validateInfoCars } from './CarsValidation';

const Form = (props) => {

    const isCreating = useSelector(store => store.car.cars);
    const isEditedCreated = useSelector(store => store.car.isEditedCreated);
    const colors = useSelector(store => store.car.colors);
    const carRedux = useSelector(store => store.car.car);
    const [isMount, setIsMount] = useState(false);
    const [snackOpen, setSnackOpen] = useState(false);
    const [snackMessage, setSnackMessage] = useState('');
    const [error, setError] = useState({});
    const [cars, setCars] = useState({
        plaque: '', price: '', model: '',
        colors: '', status: '', active: ''
    });

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(changeIsCreating(true));
    }, [dispatch]);

    useEffect(() => {
        dispatch(getColors());
    }, [dispatch]);


    const onChange = event => {
        const { name, value } = event.target;
        setCars({ ...cars, [name]: value });
    };

    const onChangeLocation = event => {
        const { name, value } = event.target;
        if (name === 'colors')
            setCars({
                ...cars, [name]: value,
            });
        else
            setCars({ ...cars, [name]: value });
    };

    const onSubmit = () => {
        const CarsError = validateInfoCars(cars);
        setError(CarsError);
        if (Object.entries(CarsError).length === 0)
            console.log(Object.entries(cars));
        dispatch(addDataCars(cars));
    };

    useEffect(() => {
        if (isEditedCreated) {
            const message = isCreating ? 'Carro guardado satisfactoriamente!' :
                'Carro actualizado satisfactoriamente!'
            setSnackOpen(true);
            setSnackMessage(message);
            // const timer = setTimeout(() => {
            //     dispatch(cancelCarCreatedUpdated());
            //     props.history.push(ROUTES.HOME_PAGE);
            // }, 1500);
            // return () => clearTimeout(timer);
        }
    }, [isEditedCreated, isCreating, props.history, dispatch]);

    const handleCloseSnackbar = (e, reason) => {
        if (reason === 'clickaway') return;
        setSnackOpen(false);
    };

    useEffect(() => {
        if (!isCreating && !isMount && Object.entries(carRedux).length > 0) {
            setCars(carRedux);
            setIsMount(true);
        }
    }, [isCreating, isMount, carRedux]);

    // useEffect(() => {
    //     const { id } = props.match.params;
    //     console.log(id);
    //     if (id) {
    //         dispatch(changeIsCreating(false));
    //         dispatch(getCarById(id));
    //     }
    // }, [dispatch, props.match.params]);

    return (
        <div style={{ margin: 30 }}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <TextFieldComponent name='plaque'
                        placeholder='Placa'
                        valueInput={cars.plaque}
                        callback={onChange.bind(this)}
                        errorInput={error.plaque}
                        title='PLACA'
                    />
                    <TextFieldComponent name='price'
                        placeholder='Precio'
                        valueInput={cars.price}
                        callback={onChange.bind(this)}
                        errorInput={error.price}
                        title='PRECIO'
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <SelectFieldComponent name='colors'
                        placeholder='Seleccione el color del carro'
                        valueInput={colors.length > 0 ? cars.colors : ''}
                        onChange={onChangeLocation.bind(this)}
                        errorInput={error.colors}
                        title='COLOR DEL VEHICULO' options={colors}
                    />
                    <div>
                        <div style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                            <input type="radio" name="status" onChange={onChange.bind(this)} value="used" />
                            <p>USADO</p>
                        </div>
                    </div>
                    <div>
                        <div style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                            <input type="radio" name="status" onChange={onChange.bind(this)} value="new" />
                            <p>NUEVO</p>
                        </div>
                    </div>
                    <p style={{
                        color: '#DE5555', fontWeight: 500,
                        fontSize: '12px',
                        lineHeight: '14px',
                    }}>
                        {error['status']}
                    </p>
                    <Button color='primary' variant='contained' onClick={onSubmit.bind(this)} style={{ alignContent: 'center !important' }}>
                        GUARDAR
                    </Button>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <TextFieldComponent name='model'
                        placeholder='Modelo'
                        valueInput={cars.model}
                        callback={onChange.bind(this)}
                        errorInput={error.model}
                        title='MODELO'
                    />
                    <FormControlLabel control={<Switch defaultChecked />} label="Activo" name='active' onChange={onChange.bind(this)} />
                </Grid>
                <Snackbar open={snackOpen}
                    autoHideDuration={4000}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                    onClose={handleCloseSnackbar}
                    message={<span>{snackMessage}</span>}
                />
            </Grid>
        </div >
    );
};

export default withRouter(Form);